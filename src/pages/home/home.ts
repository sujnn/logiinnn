import { Component } from '@angular/core';

import { NavController,AlertController } from 'ionic-angular';
import { AboutPage } from '../about/about';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
aboutpage=AboutPage;

  constructor(public navCtrl: NavController, public alertCtrl:AlertController) {
    
  }
 doAlert() {
    let alert = this.alertCtrl.create({
      title: 'Pregency App',
      message: 'This app is made special for Mother ',
      buttons: ['Ok']
    });
    alert.present()
}

}
